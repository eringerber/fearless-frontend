
function createCard(name, description, pictureUrl, starts, ends, location,) {
  return `
  <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
  <img src="${pictureUrl}" class="card-img-top">
    <div class="card">

      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class ="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
    <div class="card-footer">
    <small class="text-body-secondary">${starts} - ${ends}</small>

    </div>
    </div>

  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error('Response not ok')
    } else {
      const data = await response.json();
      let counter=0

      for (let conference of data.conferences) {

        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts=new Date(details.conference.starts);
          const ends = new Date(details.conference.ends);
          const location=details.conference.location.name;

          const html = createCard(name, description, pictureUrl, starts.toLocaleDateString(), ends.toLocaleDateString(), location);

          const columns=document.querySelectorAll('.col');
          const column=columns[counter];
          column.innerHTML +=html;
          counter++;
          if (counter===3){counter =0}

        }
      }

    }
  } catch (e) {
    if (error(e)){
      throw new Error('Error!')
    }
    // Figure out what to do if an error is raised






  }

});
